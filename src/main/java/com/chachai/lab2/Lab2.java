/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.chachai.lab2;

import java.util.Scanner;

/**
 *
 * @author Lenovo
 */
public class Lab2 {

    public int row;
    public int col;
    Scanner kb = new Scanner(System.in);
    public boolean play = false;
    public String start;
    public String[][] board = {{"_", "_", "_"}, {"_", "_", "_"}, {"_", "_", "_"}};
    public String turn;

    public void startGame() {
        System.out.println("Welcome to  OX game!!!");
        System.out.print("Start Game??? (y/n): ");
        start = kb.nextLine().toLowerCase();
        while (!start.equals("n") && !start.equals("y")) {
            System.out.print("Start Game (y/n): ");
            start = kb.nextLine().toLowerCase();
        }
        if (start.equals("n")) {
            play = false;
        } else {
            play = true;
        }

    }

    public void showBoard() {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                System.out.print(board[i][j] + " ");

            }
            System.out.println();

        }
    }

    public void showTurn() {
        System.out.println("Turn --> " + turn.toUpperCase());

    }

    public void inputRowAndCol() {
        System.out.print("Input row : ");
        row = kb.nextInt();
        System.out.print("Input column : ");
        col = kb.nextInt();
        if (((row > 0 && row < 4) && (col > 0 && col < 4))) {
            if (board[row - 1][col - 1].equals("_")) {
                board[row - 1][col - 1] = turn.toUpperCase();
                checkWin();
                checkDraw();
                nextTurn();
            } else {
                System.out.println("The selected position is already occupied. Please try again.");
            }
        } else {
            System.out.println("Invalid input. Please try again.");
        }
    }

    public void nextTurn() {
        if (turn.equals("x")) {
            turn = "o";
        } else {
            turn = "x";
        }

    }

    public boolean checkRows() {
        for (int j = 0; j < board[row - 1].length; j++) {
            if (!board[row - 1][j].toLowerCase().equals(turn)) {
                return false;
            }
        }
        return true;

    }

    public boolean checkColumns() {
        for (int j = 0; j < board[0].length; j++) {
            if (board[0][j].toLowerCase().equals(turn) && board[1][j].toLowerCase().equals(turn) && board[2][j].toLowerCase().equals(turn)) {
                return true;
            }
        }
        return false;
    }

    public boolean checkDiagonals() {
        if (board[0][0].toLowerCase().equals(turn) && board[1][1].toLowerCase().equals(turn) && board[2][2].toLowerCase().equals(turn)) {
            return true;
        }

        if (board[0][2].toLowerCase().equals(turn) && board[1][1].toLowerCase().equals(turn) && board[2][0].toLowerCase().equals(turn)) {
            return true;
        }

        return false;
    }

    public boolean checkDraw() {
        boolean isDraw = true;
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (board[i][j].equals("_")) {
                    isDraw = false;
                    break;
                }
            }
            if (!isDraw) {
                break;
            }
        }
        if (isDraw) {
            System.out.println("!!! Draw !!!");
            showBoard();
            if (playAgain()) {
                reset();
            } else {
                play = false;
            }
        }
        return isDraw;
    }

   public void checkWin() {
        if (checkRows() || checkColumns() || checkDiagonals()) {
            System.out.println("!!! " + turn + " Win !!!");
            showBoard();
            if (playAgain()) {
                reset();
            } else {
                play = false;
            }
        }
    }

    public void reset() {
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                board[i][j] = "_";
            }
        }
        turn = "o";
    }

    public boolean playAgain() {
        String again = kb.nextLine().toLowerCase();
        while (!again.equals("n") && !again.equals("y")) {

            System.out.print("Do you want to Exit ??? (y/n): ");
            again = kb.nextLine().toLowerCase();
        }
        if (again.equals("y")) {
            return false;
        }
        return true;

    }

    public void playGame() {
        showBoard();
        showTurn();
        inputRowAndCol();
    }

    public static void main(String[] args) {
        Lab2 project = new Lab2();
        Scanner kb = new Scanner(System.in);
        project.startGame();
        if (!project.play) {
            System.out.println("See you Again Bye!!");
            return;
        }
        project.turn = "x";
        while (project.play) {
            project.playGame();
        }
    }
}
